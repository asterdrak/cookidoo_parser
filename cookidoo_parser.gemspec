# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name        = 'cookidoo_parser'
  s.version     = File.file?('.gem-version') && File.readlines('.gem-version').first.strip || '0.0.0'
  s.summary     = 'Web based cookidoo parser'
  s.authors     = ['Wojciech Zakrzewski <asterdrak@gmail.com>']
  s.email       = 'asterdrak@gmail.com'
  s.files       = ['lib/cookidoo_parser.rb', 'lib/read_config.rb']
  s.homepage = 'https://modorg.pl'
  s.license = 'MIT'
  s.required_ruby_version = '~> 2.7'

  s.add_runtime_dependency 'selenium-webdriver', '~> 3.142'
end
