# frozen_string_literal: true

require 'yaml'
require 'ostruct'

class ReadConfig
  def initialize(filename)
    @filename = filename
  end

  def config_hash
    raw_config.transform_keys(&:to_sym)
  end

  private

  def raw_config
    YAML.load_file(@filename)
  end
end
