# frozen_string_literal: true

require 'selenium-webdriver'

require_relative 'read_config'

class CookidooParser
  def initialize(config, window: false)
    @config = OpenStruct.new(config.transform_keys(&:to_sym))
    @cookidoo_url = 'https://cookidoo.co.uk'
    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--headless') unless window
    add_options(options)

    client = Selenium::WebDriver::Remote::Http::Default.new(open_timeout: 60, read_timeout: 60)
    client.read_timeout = 100 # seconds

    @driver = Selenium::WebDriver.for :chrome, options: options, http_client: client
    @driver.manage.timeouts.page_load = 120
    # @wait = Selenium::WebDriver::Wait.new(timeout: 100)
  end

  def self.from_file(filename: 'user_credentials.yml', window: false)
    new(ReadConfig.new(filename).config_hash, window: window)
  end

  attr_reader :config, :driver

  def login
    visit_path
    driver.find_element(:link, 'Login').click

    form = driver.find_element(:css, '[name=j_login]')
    form.find_element(:css, '[name=j_username]').send_keys(config.email)
    form.find_element(:css, '[name=j_password]').send_keys(config.password)
    form.find_element(:css, '[name=j_submit]').click
  end

  def add_to_shopping_list(product_name)
    login
    visit_path('shopping')
    driver.find_element(:css, '.pm-add-additional-item input').send_keys(product_name)
    driver.find_element(:css, '.pm-add-additional-item button').click
    self
  end

  def quit
    driver.quit
  end

  def self.finalize(_)
    driver.quit
  end

  private

  def add_options(options)
    options.add_argument('--no-sandbox')
    # options.add_argument('--disable-dev-shm-usage')
    # options.add_argument('--disable-gpu')

    # options.add_argument('--disable-extensions')
    # options.add_argument('--no-cache')
    # options.add_argument('--window-size=1024x768')
    # options.add_argument('--user-data-dir=/tmp/user-data')
    # options.add_argument('--hide-scrollbars')
    # options.add_argument('--enable-logging')
    # options.add_argument('--log-level=0')
    # options.add_argument('--v=99')
    # options.add_argument('--single-process')
    # options.add_argument('--data-path=/tmp/data-path')
    # options.add_argument('--ignore-certificate-errors')
    # options.add_argument('--homedir=/tmp')
    # options.add_argument('--disk-cache-dir=/tmp/cache-dir')
  end

  def visit_path(path = '/')
    path = "/#{path}" unless path[0].start_with?('/')

    driver.get("#{@cookidoo_url}#{path}")
  end
end
