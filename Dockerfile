ARG FUNCTION_DIR="/function"

FROM selenium/standalone-chrome
# FROM ubuntu

RUN sudo apt update && sudo apt install -y \
  sudo g++ make cmake unzip libcurl4-openssl-dev \
  ruby ruby-dev ruby-full #chromium-chromedriver

RUN sudo gem install bundler

# Include global arg in this stage of the build
ARG FUNCTION_DIR
# Create function directory
RUN sudo mkdir -p ${FUNCTION_DIR}/gems
COPY lambda-entrypoint.sh /
COPY entrypoint.sh /
RUN sudo chmod a+x lambda-entrypoint.sh entrypoint.sh
COPY . ${FUNCTION_DIR}

RUN sudo chown -R $(whoami):$(whoami) ${FUNCTION_DIR}

ENV GEM_HOME=${FUNCTION_DIR}/gems
WORKDIR ${FUNCTION_DIR}
RUN bundle install

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
RUN sudo chmod a+x bin/add_to_shopping_list.rb
# ENTRYPOINT [ "bin/add_to_shopping_list.rb" ]

# Fix for lambda specific problem
RUN sudo sed -i "s/\/var\/log\/supervisor\/supervisord.log/\/tmp\/supervisord.log/g" /etc/supervisord.conf

ENTRYPOINT [ "/entrypoint.sh" ]
# ENTRYPOINT [ "/bin/bash" ]


# ENTRYPOINT [ "/lambda-entrypoint.sh" ]
# CMD [ "app.LambdaFunction::Handler.process" ]

# CMD [ "app.handler" ]

# FROM public.ecr.aws/lambda/ruby:2.7

# WORKDIR /app
# # RUN apk add --update make chromium chromium-chromedriver
# # RUN yum install -y epel-release
# # RUN yum install -y chromium
# RUN curl -O  https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
# RUN yum install google-chrome-stable_current_x86_64.rpm make -y

# ADD . /app
# RUN make
# RUN ["chmod", "+x", "bin/add_to_shopping_list.rb"]

# ENTRYPOINT ["bin/add_to_shopping_list.rb"]

# # FROM public.ecr.aws/lambda/ruby:2.7

# # RUN curl -O  https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
# # RUN yum install google-chrome-stable_current_x86_64.rpm make unzip wget -y

# # # Install Chromedriver
# # RUN wget https://chromedriver.storage.googleapis.com/96.0.4664.45/chromedriver_linux64.zip
# # RUN unzip ./chromedriver_linux64.zip
# # RUN rm ./chromedriver_linux64.zip
# # RUN mv -f ./chromedriver /usr/local/bin/chromedriver
# # RUN chmod 755 /usr/local/bin/chromedriver

# # COPY . ${LAMBDA_TASK_ROOT}
# # # RUN make

# # # Copy function code
# # COPY app.rb ${LAMBDA_TASK_ROOT}

# # # Copy dependency management file
# # COPY Gemfile ${LAMBDA_TASK_ROOT}

# # # Install dependencies under LAMBDA_TASK_ROOT
# # ENV GEM_HOME=${LAMBDA_TASK_ROOT}
# # RUN bundle install

# # # Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
# # CMD [ "app.LambdaFunction::Handler.process" ]
