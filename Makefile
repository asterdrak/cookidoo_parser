all:
	make clean
	make build
	make install

clean:
	rm -f cookidoo_parser-*.gem

build:
	gem build cookidoo_parser.gemspec

install:
	gem install cookidoo_parser-*.gem

start:
	irb -r pry -r cookidoo_parser

login:
	aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 691078119599.dkr.ecr.eu-central-1.amazonaws.com

make push:
	make docker-build
	docker tag cookidoo_parser:latest 691078119599.dkr.ecr.eu-central-1.amazonaws.com/cookidoo_parser:latest
	docker push 691078119599.dkr.ecr.eu-central-1.amazonaws.com/cookidoo_parser:latest

run:
	bin/add_to_shopping_list.rb xd2

docker-build:
	docker build -t cookidoo_parser .

docker-run:
	docker run --rm -it cookidoo_parser "xd-docker $(shell date)"

docker-build-run:
	make docker-build
	make docker-run
