# frozen_string_literal: true

require 'pry'

require 'read_config'

RSpec.describe ReadConfig do
  describe '.config_hash' do
    subject { described_class.new('./user_credentials.yml.default').config_hash }

    it { is_expected.to eq({ email: 'jdoe@example.com', password: 'password' }) }
  end
end
