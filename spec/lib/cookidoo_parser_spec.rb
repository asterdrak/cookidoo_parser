# frozen_string_literal: true

require 'cookidoo_parser'

RSpec.describe CookidooParser do
  describe '.new config' do
    subject { described_class.new(email: 'jdoe@example.com', password: 'password').config }

    it { is_expected.to have_attributes(email: 'jdoe@example.com', password: 'password') }
  end

  describe '.from_file' do
    subject { described_class.from_file(filename: 'user_credentials.yml.default').config }

    it { is_expected.to have_attributes(email: 'jdoe@example.com', password: 'password') }
  end
end
