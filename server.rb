require 'sinatra'
require_relative 'lib/cookidoo_parser'

get '/' do
  item = '/add?item=carrot_or_other_stuff'
  'try to run: <a href="%<item>s">%<item>s</a>' % { item: item }
end

get '/add' do
  CookidooParser.from_file.add_to_shopping_list(params['item']).quit

  "#{params['item']} was added to cookidoo list"
end
