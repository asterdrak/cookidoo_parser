# frozen_string_literal: true

require_relative 'lib/cookidoo_parser'

module LambdaFunction
  class Handler
    def self.process(event:, context:)
      CookidooParser.from_file.add_to_shopping_list('xd from image').quit
    end
  end
end
